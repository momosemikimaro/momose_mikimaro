package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Message;
import dao.DeleteDao;


public class DeleteService {

    public void delete(Message messageId) {

        Connection connection = null;
        try {
            connection = getConnection();

            DeleteDao deleteDao = new DeleteDao();
            deleteDao.delete(connection, messageId);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}