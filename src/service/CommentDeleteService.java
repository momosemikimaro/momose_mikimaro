package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.Comment;
import dao.CommentDeleteDao;


public class CommentDeleteService {

    public void delete(Comment commentId) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDeleteDao commentDeleteDao = new CommentDeleteDao();
            commentDeleteDao.delete(connection, commentId);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}