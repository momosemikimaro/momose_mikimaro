package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Store;
import dao.StoreDao;


public class StoreService {

    public List<Store> getStores() {

        Connection connection = null;
        try {
            connection = getConnection();

            StoreDao storeDao = new StoreDao();
            List<Store> ret = storeDao.getStores(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}