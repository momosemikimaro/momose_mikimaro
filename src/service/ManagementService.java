package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import dao.ManagementDao;

public class ManagementService {
	public List<User> getUser () {

        Connection connection = null;
        try {
            connection = getConnection();

           ManagementDao managementDao = new ManagementDao();
            List<User> user = managementDao.getUser(connection);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
