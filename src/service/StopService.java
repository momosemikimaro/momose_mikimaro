package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.StopDao;


public class StopService {

    public void stop(User userManagement) {

        Connection connection = null;
        try {
            connection = getConnection();

           StopDao stopDao = new StopDao();
            stopDao.stop(connection, userManagement);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}