package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.User;
import dao.ResurrectionDao;


public class ResurrectionService {

    public void resurrection(User userId) {

        Connection connection = null;
        try {
            connection = getConnection();

           ResurrectionDao resurrectionDao = new ResurrectionDao();
            resurrectionDao.resurrection(connection, userId);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}