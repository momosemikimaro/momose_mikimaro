package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.User;
import exception.SQLRuntimeException;

public class ResurrectionDao {

    public void resurrection(Connection connection, User userId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET is_deleted = 0 WHERE id = ?");

            //toStringで文字列に変換しつつpsに代入
            ps = connection.prepareStatement(sql.toString());
            //?に値を代入
            ps.setInt(1, userId.getId());

            //SQL文の実行
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}