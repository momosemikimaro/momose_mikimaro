package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

//登録画面で入力されたデータをDBにインサート
public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users (");
			sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", store_id");
            sql.append(", department_id");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // store_id
            sql.append(", ?"); // despartment_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getStoreId());
            ps.setInt(5, user.getDepartmentId());

            //何を入れようとしているか分かる
            System.out.println(ps.toString());
            System.out.println( user.getPassword());

            ps.executeUpdate();
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}

	}
	//ログインに必要なデータの取得
	public User getUser(Connection connection, String loginId,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";
        	System.out.println(sql);
            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);

            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	public User checkLoginId(Connection connection, String loginId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginId);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<User> toUserList(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String name  = rs.getString("name");
                int storeId = rs.getInt("store_id");
                int departmentId = rs.getInt("department_id");
                int isDeleted = rs.getInt("is_deleted");


                User user = new User();
                user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
                user.setStoreId(storeId);
                user.setDepartmentId(departmentId);
                user.setIsDeleted(isDeleted);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
	}

	 public void update(Connection connection, User editUser) {

	        PreparedStatement ps = null;

	        if (StringUtils.isEmpty(editUser.getPassword())) {

		        try {
		            StringBuilder sql = new StringBuilder();
		            sql.append("UPDATE users SET");
		            sql.append("  login_id = ?");
		            sql.append(", name = ?");
		            sql.append(", store_id = ?");
		            sql.append(", department_id = ?");
		            sql.append(" WHERE");
		            sql.append(" id = ?");

		            ps = connection.prepareStatement(sql.toString());

		            ps.setString(1, editUser.getLoginId());
		            ps.setString(2, editUser.getName());
		            ps.setInt(3, editUser.getStoreId());
		            ps.setInt(4, editUser.getDepartmentId());
		            ps.setInt(5, editUser.getId());

		            int count = ps.executeUpdate();
		            if (count == 0) {
		                throw new NoRowsUpdatedRuntimeException();
		            }
		        } catch (SQLException e) {
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(ps);
		        }
	        } else {

		        try {
		            StringBuilder sql = new StringBuilder();
		            sql.append("UPDATE users SET");
		            sql.append("  login_id = ?");
		            sql.append(", password = ?");
		            sql.append(", name = ?");
		            sql.append(", store_id = ?");
		            sql.append(", department_id = ?");
		            sql.append(" WHERE");
		            sql.append(" id = ?");

		            ps = connection.prepareStatement(sql.toString());

		            ps.setString(1, editUser.getLoginId());
		            ps.setString(2, editUser.getPassword());
		            ps.setString(3, editUser.getName());
		            ps.setInt(4, editUser.getStoreId());
		            ps.setInt(5, editUser.getDepartmentId());
		            ps.setInt(6, editUser.getId());

		            int count = ps.executeUpdate();
		            if (count == 0) {
		                throw new NoRowsUpdatedRuntimeException();
		            }
		        } catch (SQLException e) {
		            throw new SQLRuntimeException(e);
		        } finally {
		            close(ps);
		        }
		    }
	 }

}