package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

//DBからユーザーテーブルのデータを全て取得
public class ManagementDao {

	public List<User> getUser(Connection connection) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("stores.name as store, ");
            sql.append("departments.name as department, ");
            sql.append("users.is_deleted as is_deleted ");
            sql.append("FROM users ");
            sql.append("INNER JOIN stores ");
            sql.append("ON users.store_id = stores.id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");
            sql.append("ORDER BY stores.id");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if(userList.isEmpty() == true) {
				return null;
			} else {
				  return userList;
			}

		} catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}





	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> users = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String loginId = rs.getString("login_id");
	            String name = rs.getString("name");
	            String store = rs.getString("store");
	            String department = rs.getString("department");
	            int isDeleted = rs.getInt("is_deleted");

	            User user = new User();
	            user.setId(id);
	            user.setLoginId(loginId);
	            user.setName(name);
	            user.setStore(store);
	            user.setDepartment(department);
	            user.setIsDeleted(isDeleted);

	            users.add(user);
	            System.out.println(users);
	        }
	        return users;

	    } finally {
	        close(rs);
	    }
	}
}


