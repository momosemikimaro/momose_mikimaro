package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Search;
import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num, Search search) {

        PreparedStatement ps = null;
        String searchCategory = search.getSearchCategory();
        String searchStartDate = search.getStartDate();
    	String searchEndDate = search.getEndDate();

        try {
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.subject as subject, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("stores.name as store, ");
            sql.append("departments.name as department, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("INNER JOIN stores ");
            sql.append("ON users.store_id = stores.id ");
            sql.append("INNER JOIN departments ");
            sql.append("ON users.department_id = departments.id ");
            sql.append("WHERE created_date BETWEEN ? AND ? ");
            if(!StringUtils.isEmpty(searchCategory)){
            	sql.append("AND category LIKE '%" + searchCategory +"%'");
            }
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            if(!StringUtils.isEmpty(searchStartDate)){
            	ps.setString(1, searchStartDate + " 00:00:00");
            } else {
            	ps.setString(1, "2018-05-01 00:00:00");
            	}
            if(!StringUtils.isEmpty(searchEndDate)){
	            ps.setString(2, searchEndDate + " 23:59:59");
            } else {
            	Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            	ps.setString(2, sdf.format(date));
            }

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String store = rs.getString("store");
	            String department = rs.getString("department");
                int userId = rs.getInt("user_id");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");



                UserMessage message = new UserMessage();
                message.setId(id);
                message.setLoginId(loginId);
                message.setName(name);
                message.setStore(store);
                message.setDepartment(department);
                message.setUserId(userId);
                message.setSubject(subject);
                message.setText(text);
                message.setCategory(category);
                message.setCreatedDate(createdDate);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}