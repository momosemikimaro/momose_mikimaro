package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.User;
import exception.SQLRuntimeException;

public class StopDao {

    public void stop(Connection connection, User userManagement) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET is_deleted = ? WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            if(userManagement.getIsDeleted() == 0){
            	ps.setInt(1, 1);
            	ps.setInt(2, userManagement.getId());
            } else {
            	ps.setInt(1, 0);
            	ps.setInt(2, userManagement.getId());
            }


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}