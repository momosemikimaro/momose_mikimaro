package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class DeleteDao {

    public void delete(Connection connection, Message messageId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM messages WHERE id = ?");

            //toStringで文字列に変換しつつpsに代入
            ps = connection.prepareStatement(sql.toString());
            //?に値を代入
            ps.setInt(1, messageId.getId());

            //SQL文の実行
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}