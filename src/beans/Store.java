package beans;

import java.io.Serializable;

public class Store implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String storeName;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
}
