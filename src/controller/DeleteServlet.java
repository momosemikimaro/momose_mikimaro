package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Message;
import service.DeleteService;

@WebServlet(urlPatterns = { "/delete" })

public class DeleteServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    //詳しく学ぶ必要性あり
	    @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        request.getRequestDispatcher("home.jsp").forward(request, response);
		}

	    //削除ボタンで削除する投稿のIDを取得
	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

            Message messageId = new Message();
            int id = Integer.parseInt(request.getParameter("messageId"));
            messageId.setId(id);

            new DeleteService().delete(messageId);
            response.sendRedirect("./");
	    }
}
