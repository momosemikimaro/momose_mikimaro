package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.ResurrectionService;

@WebServlet(urlPatterns = { "/resurrection" })

public class ResurrectionServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        request.getRequestDispatcher("management.jsp").forward(request, response);
		}

	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

            User userId = new User();
            int id = Integer.parseInt(request.getParameter("userId"));
            userId.setId(id);

           new ResurrectionService().resurrection(userId);

            response.sendRedirect("./");

	    }
}
