package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Search;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		Search search = getSearch(request);

        List<UserMessage> messages = new MessageService().getMessage(search);
        List<UserComment> comments = new CommentService().getComment();

        System.out.println(comments.size());
        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("search", search);

        request.getRequestDispatcher("home.jsp").forward(request, response);
    }

	private Search getSearch(HttpServletRequest request)
			throws IOException, ServletException {

		Search search = new Search();
		search.setSearchCategory(request.getParameter("searchCategory"));
		search.setStartDate(request.getParameter("startDate"));
		search.setEndDate(request.getParameter("endDate"));

		return search;
	}

}




