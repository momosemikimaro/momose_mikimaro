package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Comment;
import service.CommentDeleteService;

@WebServlet(urlPatterns = { "/commentDelete" })

public class CommentDeleteServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    //詳しく学ぶ必要性あり
	    @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        request.getRequestDispatcher("home.jsp").forward(request, response);
		}

	    //削除ボタンで削除する投稿のIDを取得
	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

            Comment commentId = new Comment();
            int id = Integer.parseInt(request.getParameter("commentId"));
            commentId.setId(id);

            new CommentDeleteService().delete(commentId);
            response.sendRedirect("./");
	    }
}
