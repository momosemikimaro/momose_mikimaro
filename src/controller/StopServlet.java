package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.StopService;

@WebServlet(urlPatterns = { "/stop" })

public class StopServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        request.getRequestDispatcher("management.jsp").forward(request, response);
		}

	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

            User userManagement = new User();
            int id = Integer.parseInt(request.getParameter("userId"));
            userManagement.setId(id);
            int isDeleted = Integer.parseInt(request.getParameter("isDeleted"));
            userManagement.setIsDeleted(isDeleted);
           new StopService().stop(userManagement);

            response.sendRedirect("management");

	    }
}
