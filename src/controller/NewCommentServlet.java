package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		Comment tryComment = getComment(request);

		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();
			comment.setUserId(user.getId());
			comment.setMessageId(request.getParameter("messageId"));
			comment.setText(request.getParameter("comment"));
			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("tryComment", tryComment);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String comment = request.getParameter("comment");

		if (StringUtils.isEmpty(comment) == true || StringUtils.isBlank(comment)) {
			messages.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			messages.add("500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	private Comment getComment(HttpServletRequest request)
            throws IOException, ServletException {

		Comment tryComment = new Comment();
        tryComment.setText(request.getParameter("comment"));
        tryComment.setMessageId(request.getParameter("messageId"));
        return tryComment;
    }
}
