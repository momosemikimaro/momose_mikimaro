package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })

public class NewMessageServlet extends HttpServlet {
	    private static final long serialVersionUID = 1L;

	    //詳しく学ぶ必要性あり
	    @Override
	    protected void doGet(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        request.getRequestDispatcher("newMessage.jsp").forward(request, response);
		}

	    //JSPに入力されたメッセージを受け取るメソッド
	    @Override
	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws IOException, ServletException {

	        HttpSession session = request.getSession();
	        List<String> error = new ArrayList<String>();
	        Message message = getMessage(request);

	        if (isValid(request, error) == true) {

	        	//コンストラクタを利用してインスタンス生成とともにフィールドに値を渡す
	            User user = (User) session.getAttribute("loginUser");
	            //MessageBeanに格納
	            message.setUserId(user.getId());	//userbeanを利用してmessagebeanに値を格納
	            new MessageService().register(message);

	            response.sendRedirect("./");
	        } else {
	            session.setAttribute("errorMessages", error);
	            request.setAttribute("tryMessage", message);
	            request.getRequestDispatcher("newMessage.jsp").forward(request, response);

	        }
	    }
	    //入力値が間違っていないか確認するメソッド
	    private boolean isValid(HttpServletRequest request, List<String> error) {

	    	String subject = request.getParameter("subject");
	        String text = request.getParameter("text");
	        String category = request.getParameter("category");

	        if (StringUtils.isEmpty(subject) == true || StringUtils.isBlank(subject)) {
	            error.add("件名を入力してください");
	        }
	        if (30 < subject.length()) {
	            error.add("件名は30文字以下で入力してください");
	        }

	        if (StringUtils.isEmpty(text) == true || StringUtils.isBlank(text)) {
	            error.add("本文を入力してください");
	        }
	        if (1000 < text.length()) {
	            error.add("本文は1000文字以下で入力してください");
	        }

	        if (StringUtils.isEmpty(category) == true || StringUtils.isBlank(category)) {
	            error.add("カテゴリーを入力してください");
	        }
	        if (10 < category.length()) {
	            error.add("カテゴリーは10文字以下で入力してください");
	        }

	        if (error.size() == 0) {
	            return true;		//何の処理？？
	        } else {
	            return false;
	        }
	    }

	    //入力値を受け取るメソッド
	    private Message getMessage(HttpServletRequest request)
	            throws IOException, ServletException {

	        Message message = new Message();
	        message.setSubject(request.getParameter("subject"));
	        message.setText(request.getParameter("text"));
	        message.setCategory(request.getParameter("category"));
	        return message;
	 }

}
