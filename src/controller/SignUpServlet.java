package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Department;
import beans.Store;
import beans.User;
import service.DepartmentService;
import service.StoreService;
import service.UserService;

@WebServlet(urlPatterns = {"/signup"})

public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Store> storeList = new StoreService().getStores();
		request.setAttribute("storeList", storeList);
		List<Department> departmentList = new DepartmentService().getDepartments();
		request.setAttribute("departmentList", departmentList);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Store> storeList = new StoreService().getStores();
		request.setAttribute("storeList", storeList);
		List<Department> departmentList = new DepartmentService().getDepartments();
		request.setAttribute("departmentList", departmentList);

		List<String> messages = new ArrayList<String>();
		User signUpUser = getSignUpUser(request);

		if (isValid(request, messages) == true) {

			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			int storeId = Integer.parseInt(request.getParameter("storeId"));
			user.setStoreId(storeId);
			int departmentId = Integer.parseInt(request.getParameter("departmentId"));
			user.setDepartmentId(departmentId);

			new UserService().register(user);

			response.sendRedirect("management");
		} else {
			request.setAttribute("trySignUpUser", signUpUser);
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String confirmation = request.getParameter("confirmation");
		String name = request.getParameter("name");
		String storeId = request.getParameter("storeId");
		int sI = Integer.parseInt(storeId);
		String departmentId = request.getParameter("departmentId");
		int dI = Integer.parseInt(departmentId);
		User user =  new UserService().checkLoginId(loginId);

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!(loginId.matches(".*[azAZ0*9]*.") && 6 <= loginId.length() && loginId.length() <= 20)) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以下で設定してください");
		}
		if (!(user == null)) {
			messages.add("このログインIDはすでに使われています");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		} else if(!(6 <= password.length() && password.length() <= 20)) {
			messages.add("正しいパスワードを設定してください");
		} else if((!password.equals(confirmation))) {
			messages.add("パスワードが一致していません");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("氏名を入力してください");
		}
		if (StringUtils.isEmpty(storeId) == true) {
			messages.add("店舗を入力してください");
		}
		if (StringUtils.isEmpty(departmentId) == true) {
			messages.add("部署・役職を入力してください");
		}
		if (sI == 1) {
			if(!(dI == 1)) {
				if(!(dI == 2)) {
					messages.add("支店と所属・役職を確認してください");
				}
			}
		}
		if (sI == 2) {
			if(!(dI == 3)) {
				if(!(dI == 4)) {
					messages.add("支店と所属・役職を確認してください");
				}
			}
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	private User getSignUpUser(HttpServletRequest request)
            throws IOException, ServletException {

        User user = new User();

        user.setLoginId(request.getParameter("loginId"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        String stStoreId = (request.getParameter("storeId"));
        if((StringUtils.isEmpty(stStoreId) == false)){
        	int storeId = Integer.parseInt(stStoreId);
        	user.setStoreId(storeId);
        }
        String stDepartmentId = (request.getParameter("departmentId"));
        if((StringUtils.isEmpty(stDepartmentId) == false)){
        	int departmentId = Integer.parseInt(stDepartmentId);
        	user.setDepartmentId(departmentId);
        }

        return user;
	}

}
