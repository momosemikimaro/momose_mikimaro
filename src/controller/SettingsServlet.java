package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

//ユーザー新規登録機能サーブレット

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Department;
import beans.Store;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.DepartmentService;
import service.StoreService;
import service.UserService;

@WebServlet(urlPatterns = {"/settings"})
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		String id = (request.getParameter("editUserId"));

		if(id == null) {
			messages.add("不正なパラメータです");
		} else if(StringUtils.isBlank(id)) {
			messages.add("不正なパラメータです");
		} else if(!id.matches("^[0-9]+$")){
			messages.add("不正なパラメータです");
		}
		else {
			int editUserId = Integer.parseInt(id);
			User editUser = new UserService().getUser(editUserId);

			if (editUser == null) {
				messages.add("不正なパラメータです");
			} else{
				request.setAttribute("editUser", editUser);
			}
		}

		List<Store> storeList = new StoreService().getStores();
		List<Department> departmentList = new DepartmentService().getDepartments();

		session.setAttribute("errorMessages", messages);

		request.setAttribute("storeList", storeList);
		request.setAttribute("departmentList", departmentList);
		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);
		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			response.sendRedirect("management");

		} else {
			List<Store> storeList = new StoreService().getStores();
			List<Department> departmentList = new DepartmentService().getDepartments();
			session.setAttribute("errorMessages", messages);
			request.setAttribute("storeList", storeList);
			request.setAttribute("departmentList", departmentList);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		int id = Integer.parseInt(request.getParameter("id"));
		editUser.setId(id);
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setConfirmation(request.getParameter("confirmation"));
		editUser.setName(request.getParameter("name"));
		int storeId = Integer.parseInt(request.getParameter("storeId"));
		editUser.setStoreId(storeId);
		int departmentId = Integer.parseInt(request.getParameter("departmentId"));
		editUser.setDepartmentId(departmentId);

		return editUser;
	}



	//入力値が正しいかどうか確認
	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String id = request.getParameter("id");
		int editUserId = Integer.parseInt(id);
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String confirmation = (request.getParameter("confirmation"));
		String name = request.getParameter("name");
		String storeId = request.getParameter("storeId");
		int sI = Integer.parseInt(storeId);
		String departmentId = request.getParameter("departmentId");
		int dI = Integer.parseInt(departmentId);
		User user =  new UserService().checkLoginId(loginId);
		User editUser = new UserService().getUser(editUserId);

		if (StringUtils.isEmpty(id) == true) {
			messages.add("ユーザーIDを入力してください");
		}
		if (StringUtils.isEmpty(id) == true) {
			messages.add("ユーザーIDを入力してください");
		}
		if (StringUtils.isEmpty(id) == true) {
			messages.add("ユーザーIDを入力してください");
		}
		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!(loginId.matches(".*[azAZ0*9]*.") && 6 <= loginId.length() && loginId.length() <= 20)) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以下で設定してください");
		}
		if (!(user == null)) {
			if(!((loginId.equals(editUser.getLoginId())))){
			messages.add("このログインIDはすでに使われています");
			}
		}
		if (StringUtils.isEmpty(password) == false) {
			if(!(6 <= password.length() && password.length() <= 20)) {
				messages.add("パスワードは半角文字で6文字以上20文字以下で設定してください");
			} else if((!password.equals(confirmation))) {
				messages.add("パスワードが一致していません");
			}
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("氏名を入力してください");
		}
		if (StringUtils.isEmpty(storeId) == true) {
			messages.add("店舗を入力してください");
		}
		if (StringUtils.isEmpty(departmentId) == true) {
			messages.add("部署・役職を入力してください");
		}
		if (sI == 1) {
			if(dI != 1) {
				if(dI != 2) {
					messages.add("支店と所属・役職を確認してください");
				}
			}
		}
		if (sI == 2) {
			if(dI != 3) {
				if(dI != 4) {
					messages.add("支店と所属・役職を確認してください");
				}
			}
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
