package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.LoginService;


@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException{

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();


		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		User loginUser = getLoginUser(request);

		if(isValidParam(request, messages) == false) {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("tryUser", loginUser);
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		} else {


			LoginService loginService = new LoginService();
			User user = loginService.login(loginId, password);

			if(user == null) {
				messages.add("ログインに失敗しました。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("tryUser", loginUser);
				request.getRequestDispatcher("/login.jsp").forward(request, response);
				return;
			}
			if(user.getIsDeleted() == 1) {
				messages.add("ログインに失敗しました");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("tryUser", loginUser);
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			} else {
				session.setAttribute("loginUser", user);

				response.sendRedirect("./");
			}
		}
	}

	private boolean isValidParam(HttpServletRequest request, List<String> messages) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		if (StringUtils.isEmpty(loginId) == true && StringUtils.isEmpty(password) == true) {
        	messages.add("ログインIDを入力してください");
        	messages.add("パスワードを入力してください");
        	return false;

        }
        if (StringUtils.isEmpty(loginId) == true) {
        	messages.add("ログインIDを入力してください");
        	return false;

        }
        if (StringUtils.isEmpty(password) == true) {
        	messages.add("パスワードを入力してください");
        	return false;
        }

        return true;
    }

	 private User getLoginUser(HttpServletRequest request)
	            throws IOException, ServletException {

	        User user = new User();
	        user.setLoginId(request.getParameter("loginId"));
	        user.setPassword(request.getParameter("password"));
	        return user;
	 }



}

