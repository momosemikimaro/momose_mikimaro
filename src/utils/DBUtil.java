package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import exception.SQLRuntimeException;

/**
 * DB(繧ｳ繝阪け繧ｷ繝ｧ繝ｳ髢｢菫�)縺ｮ繝ｦ繝ｼ繝�繧｣繝ｪ繝�繧｣繝ｼ
 */
public class DBUtil {

	private static final String DRIVER = "com.mysql.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost/momose_mikimaro";
	private static final String USER = "root";
	private static final String PASSWORD = "ひみつ";

	static {

		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 繧ｳ繝阪け繧ｷ繝ｧ繝ｳ繧貞叙蠕励＠縺ｾ縺吶��
	 *
	 * @return
	 */
	public static Connection getConnection() {

		try {
			Connection connection = DriverManager.getConnection(URL, USER,
					PASSWORD);

			connection.setAutoCommit(false);

			return connection;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * 繧ｳ繝溘ャ繝医＠縺ｾ縺吶��
	 *
	 * @param connection
	 */
	public static void commit(Connection connection) {

		try {
			connection.commit();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	 * 繝ｭ繝ｼ繝ｫ繝舌ャ繧ｯ縺励∪縺吶��
	 *
	 * @param connection
	 */
	public static void rollback(Connection connection) {

		if (connection == null) {
			return;
		}

		try {
			connection.rollback();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

}
