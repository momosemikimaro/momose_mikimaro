<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="newMessage-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
			<c:remove var="errorMessages" scope="session" />
			</c:if>
		<div class="form-area">
		<h3>新規投稿</h3>
			<form action="newMessage" method="post">
				<label for="subject">件名</label>
				<input name="subject" value="${tryMessage.subject}" id="subject"><br /><br />
				<label for="category">カテゴリー</label>
				<input name="category" value="${tryMessage.category}" id="category"><br /><br />
				本文<br />
				<textarea name="text" cols="100" rows="15" class="text-box"><c:out value="${tryMessage.text}" /></textarea><br />
				<input type="submit" value="投稿"><br />
			</form>
			<a href="./">ホームに戻る</a>
				<div class="copyright">Copyright(c)momose.mikimaro</div>
			</div>
		</div>
	</body>
</html>