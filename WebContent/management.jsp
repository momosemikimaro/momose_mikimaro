<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
	</head>
	<body>
	<div id="wrapper">
		<div class="usersManagement">
			<div class="userManagement"><h3>ユーザー管理</h3></div>
			<br /><a class="signup" href="signup">登録する</a>
			<a class="return" href="./">ホームに戻る</a>
			<table border="1">
				<tr>
					<th>ユーザーID</th>
					<th>ログインID</th>
					<th>氏名</th>
					<th>支店</th>
					<th>部署・役職</th>
					<th>編集</th>
					<th>アカウント状態</th>
				</tr>
				<c:forEach items="${userList}" var="user">
					<tr>
						<td>
							<div class="id">
								<c:out value="${user.id}" />
							</div>
						</td>
					<td>
						<div class="login_id">
							<c:out value="${user.loginId}" />
						</div>
					</td>
					<td>
						<div class="name">
							<c:out value="${user.name}" />
						</div>
					</td>
					<td>
						<div class="store_name">
							<c:out value="${user.store}" />
						</div>
					</td>
					<td>
						<div class="department_name">
							<c:out value="${user.department}" />
						</div>
					</td>
					<td>
						<form action="settings" method="get">
							<input type="hidden" name="editUserId" value="${user.id}">
							<input type="submit" class="setting" value="ユーザー編集">
						</form>
					</td>
					<td>
						<c:if test="${user.id != loginUser.id}">
							<c:if test="${user.isDeleted == 0}">
								<div class=stop>
									<form action="stop" method="post" onclick='return confirm("このユーザーのアクセス権限を停止しますか");'>
										<input type="hidden" name="userId" value="${user.id}">
										<input type="hidden" name="isDeleted" value="${user.isDeleted}">
										<input type="submit" value="停止">
									</form>
								</div>
							</c:if>
							<c:if test="${user.isDeleted == 1}">
								<div class=resurrection>
									<form action="stop" method="post" onclick='return confirm("このユーザーのアクセス権限を復活しますか");'>
										<input type="hidden" name="userId" value="${user.id}">
										<input type="hidden" name="isDeleted" value="${user.isDeleted}">
										<input type="submit" value="復活">
									</form>
								</div>
							</c:if>
						</c:if>
						<c:if test="${user.id == loginUser.id}">
							<c:out value="ログイン中" />
						</c:if>
					</td>
					</tr>
				</c:forEach>
			</table><br />
			<div class="copyright">Copyright(c)momose.mikimaro</div>
		</div>
	</div>
	</body>
</html>