<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー登録</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="signup" method="post"><br />
			<div class="userSignup">ユーザー登録</div><br />
				<label for="loginId">ログインID</label><input name="loginId" value="${trySignUpUser.loginId}" id="loginId" /><br />
				<label for="password">パスワード</label><input name="password" type="password" id="password" /><br />
				<label for="confirmation">パスワード確認</label><input name="confirmation" type="password" id="confirmation" /><br />
				<label for="name">氏名</label><input name="name" value="${trySignUpUser.name}" id="name" /><br />
				<label for="storeId">店舗</label>
				<select name="storeId" size="1">
				<option value="">支店を選んでください</option>
				<c:forEach items="${storeList}" var="store">
				  <option value="${store.id}" <c:if test="${trySignUpUser.storeId == store.id}">selected</c:if>>${store.storeName}</option>
				</c:forEach>
				</select><br />
				<label for="departmentId">部署・役職</label>
				<select name="departmentId" size="1">
				<option value="">部署・役職を選んでください</option>
				<c:forEach items="${departmentList}" var="department">
				  <option value="${department.id}" <c:if test="${trySignUpUser.departmentId == department.id}">selected</c:if>>${department.name}</option>
				</c:forEach>
				</select>
				<c:remove var="trySignUpUser" scope="session" />
				<input type="submit" value="登録" /><br />
				<a href="management">戻る</a>
			</form>
		</div>
		<div class="copyright">Copyright(c)momose.mikimaro</div>
	</body>
</html>