<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー編集</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="setting-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="settings" method="post">
			<h3>登録ユーザー編集</h3><br />
					<input type="hidden" name="id" value="${editUser.id}"/>
					<label for="loginId">ログインID</label><input name="loginId" value="${editUser.loginId}" id="loginId" /><br />
					<label for="password">パスワード</label><input name="password" type="password" id="password" /><br />
					<label for="confirmation">パスワード確認</label><input name="confirmation" type="password" id="confirmation" /><br />
					<label for="name">氏名</label><input name="name" value="${editUser.name}" id="name" /><br />
					<label for="storeId">支店</label>
					<c:if test="${loginUser.id != editUser.id}">
						<select name="storeId" size="1">
							<c:forEach items="${storeList}" var="store">
						  		<option value="${store.id}" <c:if test="${editUser.storeId == store.id}">selected</c:if>>${store.storeName}</option>
							</c:forEach>
						</select><br />
					</c:if>
					<c:if test="${loginUser.id == editUser.id}">
						<c:forEach items="${storeList}" var="store">
								<span class="store"><c:if test="${editUser.storeId == store.id}"><c:out value="${store.storeName}" /></c:if></span>
						</c:forEach>
						<input type="hidden" name="storeId" value="${editUser.storeId}"/>
						<br />
					</c:if>
					<label for="departmentId">部署・役職</label>
					<c:if test="${loginUser.id != editUser.id}">
						<select name="departmentId" size="1">
							<c:forEach items="${departmentList}" var="department">
							  <option value="${department.id}" <c:if test="${editUser.departmentId == department.id}">selected</c:if>>${department.name}</option>
							</c:forEach>
						</select>
					</c:if>
					<c:if test="${loginUser.id == editUser.id}">
						<c:forEach items="${departmentList}" var="department">
								<span class="department"><c:if test="${editUser.departmentId == department.id}"><c:out value="${department.name}" /></c:if></span>
						</c:forEach>
						<input type="hidden" name="departmentId" value="${editUser.departmentId}"/>
						<br />
					</c:if>
					<input type="submit" value="登録" /><br />
					<a href="management">戻る</a>
			</form>
			<div class="copyright">Copyright(c)momose.mikimaro</div>
		</div>
	</body>
</html>