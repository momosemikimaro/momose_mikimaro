<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="./js/jQuery.min.js"></script>
	</head>
	<body>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
					<c:remove var="errorMessages" scope="session" />
	</c:if>
		<div class="user-contents">
				<div class="header">
					<a  class="logout "href="logout">ログアウト</a>
					<div class="homeTitle"><h1>HOME</h1></div>
					<div class="hello"><c:out value="ようこそ、${loginUser.name}さん"/></div><br />
					<div class="newMessage"><a href="newMessage" class="new">新規投稿</a></div>
						<c:if test="${loginUser.departmentId == 1 && loginUser.storeId == 1}">
							<div class="managementS"><a href="management" class="new">ユーザー管理</a></div>
						</c:if>
				</div>
				<div class="search-form-area"><br />
					<form action="./" method="get">
						<div class="search">-投稿検索-<br /></div>
						<label for="category">カテゴリー</label>
						<input name="searchCategory" value="${search.searchCategory}" id="category"><br />
						<label for="date">投稿日時</label>
						<input type="date" name="startDate" value="${search.startDate}"> ～
						<input type="date" name="endDate" value="${search.endDate}"><br />
						<input type="submit" value="検索">
					</form>
					<a href="./">リセット</a>
					<br />
				</div>
		</div>
		<div class="messages">
			<c:forEach items="${messages}" var="message">
				<div class="message">
					<div class="subject">
						<c:out value="${message.subject}" />
					</div>
					<div class="text">
						<br />
						<c:forEach var="text" items="${fn:split(message.text,'
						')}">
							<c:out value="${text}"/><br />
						</c:forEach>
					</div><br />
					<div class="category">
						カテゴリ：<c:out value="${message.category}" />
					</div>
					<div class="user-name"><br />
						<span class="name"><c:out value="投稿者：${message.name}" /></span><br />
						<span class="store"><c:out value="所属：${message.store}" /></span>
						<span class="department"><c:out value="${message.department}" /></span>
					</div>

					<div class="date">
						<fmt:formatDate value="${message.createdDate}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
				</div>

				<div class="message-delete">
					<c:if test="${message.userId == loginUser.id}">
						<form action="delete" method="post" onclick='return confirm("投稿を削除しますか");'>
							<input type="hidden" name="messageId" value="${message.id}">
							<input type="submit" value="削除">
						</form>
						<br />
					</c:if>
				</div>

				<div class="comments">
					<br /> <div class="under">コメント一覧</div><br />
					<c:forEach items="${comments}" var="comment">
						<div class="comment">

							<c:if test="${message.id == comment.messageId}">
								<div class="come">
									<div class="user">
										<span class="name"><c:out value="投稿者：${comment.name}" /></span><br />
										<span class="store"><c:out value="所属：${comment.store}" /></span>
										<span class="deoartment">
										<c:out value="${comment.department}" /></span>
									</div><br />
									<div class="commentText">
										<c:forEach var="text" items="${fn:split(comment.text,'
										')}">
											<c:out value="${text}"/><br />
										</c:forEach>
									</div>
									<div class="date">
										<br /><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" />
									</div>
								</div>
								<div class="comment-delete">
									<c:if test="${comment.userId == loginUser.id}">
										<form action="commentDelete" method="post" onclick='return confirm("コメントを削除しますか");'>
											<input type="hidden" name="commentId" value="${comment.id}">
											<input type="submit" value="削除">
										</form>
									</c:if>
								</div>
							</c:if>
						</div>
					</c:forEach>
					<div class="comment-form-area">
						<form action="newComment" method="post">
							<br />コメント<br />
							<input type="hidden" name="messageId" value="${message.id}">
							<textarea name="comment" cols="100" rows="6" class="text-box"><c:if test="${tryComment.messageId == message.id}"><c:out value="${tryComment.text}" /><c:remove var="tryComment" scope="session"/></c:if></textarea><br />
							<input type="submit" value="送信">
						</form><br />
					</div>
				</div>
			</c:forEach>
		</div>
	<div class="copyright">Copyright(c)momose.mikimaro</div>
	</body>
</html>