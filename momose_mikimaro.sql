-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: momose_mikimaro
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `text` varchar(500) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (2,2,1,'確認しました！','2018-06-01 05:58:53','2018-06-01 05:58:53'),(5,2,4,'						\r\n							4','2018-06-07 08:40:54','2018-06-07 08:40:54'),(6,4,8,'こめ\r\nんと','2018-06-11 07:14:19','2018-06-11 07:14:19'),(7,5,13,'コメント\r\nコメント\r\nコメント','2018-06-13 04:17:42','2018-06-13 04:17:42'),(8,5,13,'コメント\r\nコメント','2018-06-13 04:23:02','2018-06-13 04:23:02'),(9,5,13,'コメントコメントコメントコメント','2018-06-13 04:31:05','2018-06-13 04:31:05'),(10,5,12,'コメントコメント\r\n','2018-06-13 04:31:47','2018-06-13 04:31:47'),(11,5,14,'コメント\r\nコメント','2018-06-13 04:32:46','2018-06-13 04:32:46'),(13,5,15,'おはよう\r\nござい\r\nます','2018-06-13 04:35:56','2018-06-13 04:35:56'),(14,5,16,'こんば\r\nんは','2018-06-13 04:39:36','2018-06-13 04:39:36'),(15,5,16,'おはよ\r\nううう','2018-06-13 04:39:58','2018-06-13 04:39:58'),(16,5,1,'こめ\r\nんと','2018-06-13 05:27:08','2018-06-13 05:27:08'),(17,5,16,'     ','2018-06-14 00:19:54','2018-06-14 00:19:54'),(18,11,18,'<a href=\'./\'>LINK<a/>\r\n<a href=\'./\'>LINK<a/>\r\n','2018-06-14 00:32:17','2018-06-14 00:32:17'),(19,11,18,'<a href=\'./\'>LINK<a/><a href=\'./\'>LINK<a/><a href=\'./\'>LINK<a/>','2018-06-14 00:32:31','2018-06-14 00:32:31'),(20,11,18,'<a href=\'./\'>LINK<a/>','2018-06-14 00:32:52','2018-06-14 00:32:52'),(21,11,18,'＠＠＠＠＠','2018-06-14 00:33:04','2018-06-14 00:33:04'),(22,12,18,'admin02admin02admin02','2018-06-14 00:46:05','2018-06-14 00:46:05'),(23,12,18,'admin02admin02admin02','2018-06-14 00:46:25','2018-06-14 00:46:25'),(24,11,18,'admin02admin02','2018-06-14 00:46:54','2018-06-14 00:46:54'),(25,12,19,'admin02admin02admin02admin02admin02','2018-06-14 00:47:30','2018-06-14 00:47:30'),(26,12,19,'admin02admin02admin02admin02','2018-06-14 00:47:38','2018-06-14 00:47:38'),(27,11,19,'aaaaaaaaaaaaaaaaaaaaaaa','2018-06-14 00:47:48','2018-06-14 00:47:48'),(29,12,18,'ｑｑｑｑｑｑｑｑｑｑｑｑｑｑｑｑｑｑｑｑｑ','2018-06-14 00:49:26','2018-06-14 00:49:26'),(33,5,17,'こんにちは\r\nこんにちは\r\nこんにちは','2018-06-14 01:06:53','2018-06-14 01:06:53'),(38,1,20,'うんうん\r\nうん','2018-06-14 02:51:33','2018-06-14 02:51:33'),(39,1,20,'おん\r\nおん','2018-06-14 02:54:22','2018-06-14 02:54:22'),(40,1,20,'おん\r\nおん','2018-06-14 02:54:22','2018-06-14 02:54:22'),(41,1,20,'こんん\r\nこん','2018-06-14 03:00:06','2018-06-14 03:00:06'),(42,1,20,'こん\r\n','2018-06-14 03:00:22','2018-06-14 03:00:22'),(44,1,19,'こん\r\nこん\r\nここここおん','2018-06-14 03:55:17','2018-06-14 03:55:17'),(49,5,19,'gggffgfgfgfgfg\r\njgghhghg','2018-06-14 05:54:55','2018-06-14 05:54:55'),(50,1,22,'気になる！','2018-06-14 07:08:01','2018-06-14 07:08:01'),(51,1,22,'楽しみ！','2018-06-18 02:10:18','2018-06-18 02:10:18'),(52,5,22,'感慨深いわね','2018-06-18 02:11:22','2018-06-18 02:11:22'),(53,1,23,'ワールドカップが待ち遠しい！','2018-06-18 04:47:37','2018-06-18 04:47:37'),(55,1,22,'テストテストテストテスト\r\nテストテストテストテスト\r\nテストテストテストテスト','2018-06-18 05:37:54','2018-06-18 05:37:54'),(56,1,25,'コメント\r\nコメント\r\nコメント','2018-06-18 06:30:42','2018-06-18 06:30:42');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'総務部'),(2,'情報管理部'),(3,'支店長'),(4,'社員');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `subject` varchar(30) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `category` varchar(10) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,1,'商品の件','商品が値上げしました','商品','2018-05-31 01:40:47','2018-05-31 01:40:47'),(2,3,'送付の件','遅れます。','送付','2018-06-04 01:26:15','2018-06-04 01:26:15'),(3,1,'移転','移転します。','移転','2018-06-05 07:11:51','2018-06-05 07:11:51'),(4,1,'テスト','テスト投稿','テスト','2018-06-06 04:51:40','2018-06-06 04:51:40'),(6,2,'あああああ','さああ\r\n','ああああああ','2018-06-08 00:52:02','2018-06-08 00:52:02'),(8,4,'123456','テスト\r\n本文','カテゴリ','2018-06-11 07:12:41','2018-06-11 07:12:41'),(16,5,'挨拶','こんに\r\nちは','挨拶','2018-06-13 04:39:24','2018-06-13 04:39:24'),(17,11,'ｔｔｔｔ','ｆｆｆｆｆｆｖｖｖｖｖ','ｓｓｓ','2018-06-14 00:31:44','2018-06-14 00:31:44'),(18,11,'test','<a href=\'./\'>LINK<a/>\r\nリンクテスト','テスト','2018-06-14 00:32:09','2018-06-14 00:32:09'),(19,12,'admin02','admin02','admin02','2018-06-14 00:47:26','2018-06-14 00:47:26'),(22,5,'クレヨンしんちゃん、新声優に小林由美子さん','テレビ朝日は１４日、アニメ「クレヨンしんちゃん」の主人公・野原しんのすけの新声優が小林由美子さんに決まったと発表した。\r\n\r\n　しんのすけ役は、声優の矢島晶子さんが１９９２年４月の放送開始以来担当していたが、声を保つのが難しくなったとして降板を発表していた。\r\n\r\n　小林さんは「大役を仰せつかり光栄と共に責任の重さを痛感する毎日です。矢島さんのしんちゃん魂をしっかり学び、一年でも早く皆様に慣れ親しんで頂けるしんちゃんになれるよう、日々精進していきたいと思います」とコメントしている。\r\n\r\n　７月６日の放送から、小林さんが声優での放送となる。小林さんは、これまでにアニメ「デュエルマスターズ！」の切札（きりふだ）ジョー役、「映画ドラえもん　新・のび太と鉄人兵団～はばたけ天使たち～」のピッポ役などを担当してきた。','アニメ','2018-06-14 07:06:31','2018-06-14 07:06:31'),(23,5,'コロンビアサッカーは日本の武士道学んでいた…','コロンビアは１４年ブラジル大会で初の８強入り。２大会連続６度目となるＷ杯で、史上初の４強を狙う同国の国民性や性格は、どういったものなのか。昨年１１月、同国を訪問した元日本代表ＭＦ北澤豪氏（スポーツ報知評論家）に実情を聞いた。\r\n\r\n　北澤氏がコロンビアを訪問し最も驚いたのは、日本を“教科書”にしていたことだ。同国サッカー協会の関係者と会食した際、若手の年代別代表に見せているという一本のＤＶＤを見た。なんとその内容は、柔道など日本の武道などの礼節をまとめたものだった。\r\n\r\n　同関係者は「日本の武道ではきちんとした礼儀があり、誰もが規律を守る。日本人にあってコロンビアにないのは規律。コロンビアサッカーに規律を取り入れるために作成した」と説明。北澤氏は「もっとこのＤＶＤを良いものにしたい」と感想を求められ、しつこく日本文化について聞かれた。「コロンビアのサッカー協会が、日本の武道を参考にしているとは思わなかった。強化するために、さまざまな点に注目して参考にする。非常に勉強熱心だと感じましたし、驚きましたね」と北澤氏は振り返る。\r\n\r\n　日本障がい者サッカー連盟（ＪＩＦＦ）で会長を務める北澤氏は、当地で障がい者サッカーも視察。「コロンビアでは健常者と障がい者が当たり前のように、一緒にサッカーを楽しんでいる。これは日本ではなかなか見られない。また日本では、例えば切断障がいや視覚障がいなどで分かれたりしますが、コロンビアでは分かれることもなく一緒にサッカーをしている。これも驚きでした」。サッカー熱の高さを感じたという。\r\n\r\n　驚きは続く。北澤氏が元日本代表ＭＦだと知ると、テレビ局関係者からせがまれ、急きょ“特別ゲスト”として現地のテレビ番組に出演することに。Ｗ杯開幕まで半年以上も時間があったが、対戦国の日本に注目していた。当時から、現地はＷ杯の話題で持ちきりだった。１４年ブラジル大会では４―１で一蹴した日本からも学ぼうとする姿勢と情熱。初戦のライバルは、一筋縄ではいかなそうだ。','スポーツ','2018-06-18 02:58:53','2018-06-18 02:58:53');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (1,'本社'),(2,'支店A'),(3,'支店B'),(4,'支店C'),(5,'支店D'),(6,'支店E'),(7,'支店F');
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `syohin`
--

DROP TABLE IF EXISTS `syohin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `syohin` (
  `ID` char(1) NOT NULL,
  `NAME` varchar(32) NOT NULL,
  `PRICE` int(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `syohin`
--

LOCK TABLES `syohin` WRITE;
/*!40000 ALTER TABLE `syohin` DISABLE KEYS */;
INSERT INTO `syohin` VALUES ('1','syohin1',3000),('2','syohin2',2000),('3','syohin3',1000),('4','syohin4',1500);
/*!40000 ALTER TABLE `syohin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(10) NOT NULL,
  `store_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'momosemikimaro','lyqPqB6wNRCaYDRrFD3Xgy1CDdN0pFSs3V-sc6oRCXY','百瀬幹麿',1,1,0),(2,'aaaaaa','7QJFe1xB2WTb0vKmCdY_4bt1KNvlXhq_W1LCSc1zV5c','あああああ',5,1,0),(3,'bbbbb','XoRsZPLbEiZua2WKjltbQswiVBmz7h_KiKy7GB3f21I','びびびびび',3,3,0),(4,'alh1','kb5oymDMEEKEK0-OvSRMlVMfrtMIf-iWloo781rltvU','alh1',3,1,0),(5,'alh2alh2alh2','aA2qkiR2NBDVjSDDLWCAA8TQ1oWQZLNTLM949mVmAkw','alh2',3,1,0),(6,'mikimaromomose','OyYTnu0koLj3vW4FusTfJXM7kBh_USyQEK0UBtDbUZY','ももせ',5,5,0),(7,'alhalhalh','cR67bZIBXKelHqdWerzs2sruNcvSjYmNqtR12FAFxtY','alhalhalh',3,1,0),(8,'alh3alh3alh3','gftFcEngIrA6wDhJ-GwyXK3w4A8I2RH8yQUKvnCn6sY','alh3',2,2,0),(9,'alh4alh4alh4','Owo5K-H2izJwNZ0Uq3jboUgd6hFiE_w-rnZfmWVoCKE','alh4',4,3,0),(10,'alh55555','Td1PLGuRFYVMeDYo0Qh2lW8BOOy-AAsUKRc_kQJG2ac','alh5',3,1,0),(11,'admin01','jYl-RUsDyjafYmnSMQjYq5Z83a4rk7VJNYtrh84hxqw','総務人事部ｓｗ',3,2,0),(12,'admin02','JDpaT046J96GOR8HzwWKewtzzkYGm_hN90kwYphWD08','admin02',1,1,0),(13,'alh66666','oMGkcWcB7SnO_hq6cXq-zECaMKKYMKeJlMbRWFWI7qk','alh6',3,2,0),(14,'alhmater','vP04fk6Zvn1kxUBCgkgMwfqS3QIa47pAUbf8MFD0a_w','alhmaster',3,1,1),(15,'momose','WZvgUT9eaOJZnJJtnj4RUHujZlMu0k8VV75NB_xeVJE','momose',1,1,0),(16,'testuser','rl3rgi4NcZkpAEcacZnQ2VuOfJ0FxAqCRaKB_SwdZoQ','testuser',1,3,0),(17,'alhtest2','Mt7goP3ukaJD-isG0wrhtuGveKpQYwchrZD4OjnMXNM','alhtest2',1,1,1),(18,'testuser2','koBzaePtIA-EhkcVXBa_cbCUH1tIJ-nG1VLpIrpRrN8','testuser2',1,3,0),(19,'testuser3','E8CZYvMmWMYZDMu1AxNCn87q78roPA2v442d8eYMBOI','testuser3',3,1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-29 11:10:25
